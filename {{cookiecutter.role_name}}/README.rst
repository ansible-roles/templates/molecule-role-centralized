{{cookiecutter.role_name | upper}}
{% for letter in cookiecutter.role_name %}={% endfor %}

{{ cookiecutter.description }}

Requirements
------------

- Ansible {{cookiecutter.min_ansible_version}}+

Pipeline
--------

The pipeline for this role is centralized, as seen in .gitlab-ci.yml:
  ---
  include:
    remote: 'https://code.vt.edu/ansible-roles/molecule/-/raw/master/.gitlab-ci.yml'

If a custom pipeline is desired, edit .gitlab-ci.yml, but be warned that future updates to
centralized pipeline repository will not be automatically included in this role if modified.

The pipeline directory allows for customization of the variables and tests for the
pipeline that are specific to this role. See the README in the pipeline directory in this project
for additional details.

Role Variables
--------------

An example variable.

  {{cookiecutter.role_name}}_variable1

Another example variable.

  {{cookiecutter.role_name}}_variable2

Dependencies
------------

None

Example Playbook
----------------

The following example shows how this role can be defined in a playbook with parameters passed to override default variables.

.. code-block:: yaml

    - hosts: servers
      roles:
        - role: {{cookiecutter.role_name}}
          {{cookiecutter.role_name | replace('.','_') | replace('-','_')}}_variable1: true
          {{cookiecutter.role_name | replace('.','_') | replace('-','_')}}_variable2: false
          tags: {{cookiecutter.role_name}}
